/**
 * Created by sverre on 06.02.2017.
 */

//Getting koa and koa-tools
var koa = require('koa');
var router  = require('koa-router')();
var parse = require('co-body');

//Initiate koa
var app = new koa();

//Getting the mongodb driver
var db = require('mongodb').MongoClient;

//Adding the mongodb driver to the app context
app.context.db=db;


//Parse the body
app.use(function*(next){
    this.request.body = yield parse.json(this);
    yield next;
});


//Get request for getting a list of items
router.get('/api/rhubarbs/',function*(next){

    //Connect to the database named "Rhubarb"
    var connection = yield this.db.connect('mongodb://localhost:27017/rhubarb');

    //Do the query. Empty query ({}) returns all items
    var result = yield connection.collection('logs').find({}).toArray();

    //Adding the result to the body. This also generates a 200 status automatically.
    this.body=result;

});

//A put request to the list
router.put('/api/rhubarbs/:id',function*(next){
    var id = this.params.id;
    var connection = yield this.db.connect('mongodb://localhost:27017/rhubarb');
    var object=this.request.body;
    var result = yield connection.collection('logs').insert(object);
    this.body=object._id;

});

//A Delete request to the items
router.delete('/api/rhubarbs/:id',function*(next){
    var connection = yield this.db.connect('mongodb://localhost:27017/rhubarb');
    var id = this.params.id;
    var result = yield connection.collection('logs').deleteOne({_id:id});
    this.body=result;
    if(result.n===1){
        this.body="OK";
    }
});


//Using the routes
app.use(router.routes());

//Start up the app and listen to port 3000
app.listen(3000);
